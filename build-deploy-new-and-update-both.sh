#!/bin/bash
DOCKER_IMAGE_VERSION="1.0.12"
DOCKER_ACCOUNT="monirul87"

echo "Building docker image:"

docker build -t $DOCKER_ACCOUNT/$DOCKER_IMAGE_VERSION:latest .

sleep 10

echo "Pushing docker image to docker registry:"

docker push $DOCKER_ACCOUNT/$DOCKER_IMAGE_VERSION:latest

sleep 5

echo "Removing previous deployment, service and database:"

kubectl delete -f kubernetes/

sleep 15

echo "Deploying airasia app and postgres database:"
kubectl create -f kubernetes/

echo "Please verify after few seconds later."
