# Kubernetes Deployment 
 Single bash command script to:

1. build a tagged Docker image
2. push it into Docker repository
3. force Kubernetes deployment within selected namespace


## Assumptions
1. Docker image name is the same as Kubernetes deployment name.
2. `kubectl` is authorized to connect to Kubernetes cluster
3. If using Docker private repository - we need to authorized already with `docker login`
4. Mention appropriate release name and container name for the file build-deploy-update.sh (Example, export RELEASE_NAME="airasia-backend-deployment"). We can use this file only for new update.
5. We can use script 'build-deploy-new-and-update-both.sh' for buid image, deploy image and database.

## Installation

## Option-1: Both new deploy and update.

1. Copy `build-deploy-new-and-update-both.sh` into your executable environment $PATH.
*example:*
```
cp ./build-deploy-new-and-update-both.sh /usr/local/bin/build
```
2. Change the image name in deploymet yaml that we want to change. So both files (script and deployment file) should be same version of image.
```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: airasia-backend-deployment
  namespace: stg
spec:
  replicas: 2
  selector:
    matchLabels:
      name: airasia-backend
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 50%
      maxSurge: 1
  template:
    metadata:
      labels:
        name: airasia-backend
    spec:
      containers:
        - name: airasia-app
          imagePullPolicy: Always
          image: monirul87/1.0.12:latest
          resources:
            requests:
              cpu: "50m"
              memory: "128Mi"
```
```bash
#!/bin/bash
DOCKER_IMAGE_VERSION="1.0.12"
DOCKER_ACCOUNT="monirul87"

echo "Building docker image:"

docker build -t $DOCKER_ACCOUNT/$DOCKER_IMAGE_VERSION:latest .

sleep 10

echo "Pushing docker image to docker registry:"

docker push $DOCKER_ACCOUNT/$DOCKER_IMAGE_VERSION:latest

sleep 5

echo "Removing previous deployment, service and database:"

kubectl delete -f kubernetes/

sleep 15

echo "Deploying airasia app and postgres database:"
kubectl create -f kubernetes/

echo "Please verify after few seconds later."
```
 
Sample Output:
![Screenshot 2022-05-15 at 4.59.55 AM.png](./Screenshot 2022-05-15 at 4.59.55 AM.png)
![Screenshot 2022-05-15 at 5.00.59 AM.png](./Screenshot 2022-05-15 at 5.00.59 AM.png)


## Option-2: For update only.
1. Copy `build-deploy-only-update.sh` into your executable environment $PATH.

*example:*
```
cp ./build-deploy-only-update.sh /usr/local/bin/build
```

2. Add required environment variables listed in `.env` file into the project `.env` where `Dockerfile` is located
```bash
# required environment variables
# place it within the same folder as your Dockerfile
DOCKER_IMAGE="1.0.9"
DOCKER_ACCOUNT="monirul87"
NAMESPACE_QA="stg"
NAMESPACE_PROD="prod"
```

*example:*
```
cat ./env >> /path_to_project_where_Dockerfile_is_located/.env
```
3. Change release name and container name as per deployment defination file. 
```
export RELEASE_NAME="airasia-backend-deployment"
export CONTAINER_NAME="airasia-app"
```

Sample Output:
![Screenshot 2022-05-15 at 5.06.07 AM.png](./Screenshot 2022-05-15 at 5.06.07 AM.png)

## Usage
```
build --help
```

*examples:*

build a Docker image tagged as `latest` & push it into Docker repository:
```
build
```

build a Docker image tagged as `1.0.15`, push it into Docker repository and  Kubernetes deployment to use it in staging namespace:
```
build 1.0.15 stg
```
